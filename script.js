// let movies = ["Godfather", "Mission Impossible", "Avengers", "Venom", "The Nun"];
// console.log(movies[0]); //Godfather
// console.log(movies[1]); //Mission Impossible
// console.log(movies[2 ]); //Avengers

// console.log(movies.length); //length is 5
// console.log (movies.length -1)


// movies[1] = "Halloween"
// console.log(movies[1]); //Change Mission Impossible to Halloween

// //We can initializa an empty array two ways:

// let colors = [];
// let colors = new Array() //uncommon

// //Arrays can hold any type of data
// let random_collection = [49, true, "Hermione", null];

// //Arrays have a length property
// let nums = [45, 37, 89, 24];
// nums.length //4

//Push Method
// let colors = ["red", "orange", "yellow"];
// console.log(colors) //output is ["red", "orange", "yellow"]
// colors.push("green");
// console.log(colors) //output is ["red", "orange", "yellow", "green"]

//Pop - removing last index
// let colors = ["red", "orange", "yellow"];
// colors.pop(); //removed yellow
// console.log(colors);

//Unshift
// let colors = ["red", "orange", "yellow"];
// colors.unshift("infrared"); //["infrared", "red", "orange", "yellow"]
// console.log(colors);

// //shift
// let colors1 = ["red", "orange", "yellow"];
// console.log(colors1.shift());
// console.log(colors1);

// //indexOf
// let tuitt = ["Charles", "Paul", "Sef", "Alex", "Paul"];
// //returns the first undex at which a given element can be found
// console.log(tuitt.indexOf("Sef")); //2
// //finds the first instance of Paul not 4
// console.log(tuitt.indexOf("Paul")); //1
// //returns -1 if the element is not present
// console.log(tuitt.indexOf("Hulk")); //-1

//ARRAY ITERATIONS
// let colors = ["red", "orange", "yellow", "green"];
// for(let i = 0; i < colors.length; i++){ //or console.log(`${colors[i]} is in ${i}`) - template literals
// 	console.log(colors[i] + " is in " + i);
// }

// //FOR EACH
// let colors = ["red", "orange", "yellow", "green"];
// colors.forEach(function(color){
// 	console.log(color);

// });

// const colors = ['red', 'green', 'blue']
// //address 23 new st.
// let colors2 = ['red', 'green', 'blue']
// //address 167 times new roman st.
// let colors3 = colors;

// const scores = [
// 	[12, 15, 16],
// 	[11, 9, 8],
// 	[1, 5, 6],
// ]

// scores [1] //accessing [12, 15, 16]
// scores [1][2] //accessing [8]

//for each
// scores.forEach(function(score){
// 	console.log(score)
// })

//for loop - multi dimensional
// for(let i = 0; i < scores.length; i++){
	// console.log(scores[i])
	//scores[0][0] //accessing #12
	//another loop for scores[i]
// 	for(let j = 0; j < scores[i].length; j++){
// 		console.log(scores[i][j])
// 	}
// }


//OBJECTS - Organizing data structure
// const grades = [98, 87, 91, 84]

// const myGrades= {
// 	lastName: 'Zamora',
// 	firsName: 'Rie',
// 	programming: 84,
// 	math: 87,
// 	english: 91,
// 	science: 98,
// 	hobbies:['coding', 'writing', 'cosplay']
// }

// Key Value pairs

let person = {
	lastName: 'John',
	firsName: 'Smith',
	email: ['jsmith@gmail.com', 'js@gmail.com'],
	address:{
		city: 'Tokyo',
		country: 'Japan',
	},
	greeting: function(){ //Methods
		return 'hi';
	}
};

const shoppingCart = [
	{
		product: 'shirt'
		price: 99.90
		qty: 2
	},
	{
		product: 'watch'
		price: 9.90
		qty: 1
	},
	{
		product: 'laptop'
		price: 999.90
		qty: 2
	},
]